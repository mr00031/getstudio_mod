<?php
/**
 * @file
 * Contains \Drupal\hello_world\Controller\HelloController.
 */
namespace Drupal\getstudio\Controller;
class GetstudioController {
  public function content() {
    return array(
      '#type' => 'markup',
      '#markup' => t('All Modules Enabled'),
    );
  }
}